function Get-LocalVersion {
$ModulePath = $(Get-Variable MyInvocation).Value.PSScriptRoot 
$Module = $ModulePath | Split-Path -leaf 
Write-Output "Module Name: $Module" 
$LocalVersion = $(Get-Module -ListAvailable | Where-Object {$_.Name -eq $Module}).Version.ToString() 
return $LocalVersion
}
fucnction Get-RemoteVersion {

}

$ModuleURI = "Path.to.website" 
$LocalVersion = Get-LocalVersion


Write-Output "Current Version: $LocalVersion" 
Write-Output "Checking for Module Updates...." 

#Get Files 
(Invoke-WebRequest -Uri "https://gitlab.com/rolston/PSModuleUpdate/raw/master/PSModuleUpdate/PSModuleUpdate.psd1").Content -OutFile "$ModulePath\temp.psd1" 
Invoke-WebRequest -Uri "$ModuleURI\$Module.psm1" -OutFile "$ModulePath\temp.psm1" 
$WebVersion = $(select-string -Path "$ModulePath\temp.psd1" -pattern "ModuleVersion").Line.Split("'")[1] 
Write-Output "Online Version: $WebVersion" 
If([version]$WebVersion -gt [version]$LocalVersion) { 
    Write-Output "Updating..." 
    Move-Item "$ModulePath\$Module.psm1" "$ModulePath\$Module.psm1.$LocalVersion" 
    Move-Item "$ModulePath\$Module.psd1" "$ModulePath\$Module.psd1.$LocalVersion" 
    Move-Item "$ModulePath\temp.psm1" "$ModulePath\$Module.psm1" 
    Move-Item "$ModulePath\temp.psd1" "$ModulePath\$Module.psd1" 
}


